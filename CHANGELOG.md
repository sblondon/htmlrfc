# Changelog

## [1.2] - 2023-03-24

- Plug-in enabled on rfc-editor.org website
- Redirect to HTML RFC on rfc-editor.org website

## [1.1] - 2019-09-11

- Link can be hidden
- Prettier link

## [1.0.0] - 2019-09-03

_Initial release._