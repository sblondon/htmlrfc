# htmlrfc: a plug in for Firefox

Add a link to HTML page RFC if the text version is shown.
It works on ietf.org website only.

Released under GPL licence v3 or upper

Demo:
- enable the plugin
- go to https://www.ietf.org/rfc/rfc2045.txt

Documentation about Firefox plugin
https://developer.mozilla.org/fr/docs/Mozilla/Add-ons/WebExtensions/Your_first_WebExtension
