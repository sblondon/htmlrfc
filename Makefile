archive: clean
	zip -r -FS ../htmlrfc.zip * --exclude *.git* --exclude icons/icon.svg --exclude Makefile --exclude CHANGELOG.md

clean:
	rm -rf *~
