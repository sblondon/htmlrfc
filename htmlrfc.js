function close_action(){
    const elem = document.getElementById('propose-version')
    elem.parentNode.removeChild(elem)
}

function main(){
    add_style()
    add_url()
}

function add_style(){
    const head = document.querySelector('head');
    const css = [
        '#propose-version {position: relative; padding: .75rem 1.25rem; border: 1px solid transparent; border-radius: .25rem; color: #0c5460; background-color: #d1ecf1; border-color: #bee5eb; max-width: 20rem; box-sizing: border-box;}',
        '#html-link {color: blue; font-family: sans-serif;}',
        '#close-html-version-button {cursor: pointer; position: absolute; top: 0; right: 0; font-size: 1.5rem; font-weight: 800; color: #000; text-shadow: 0 1px 0 #fff; opacity: .5;}'
    ]
    head.innerHTML += '<style>' + css.join(' ') + '</style>'
}

function add_url(url){
    const alt_url = html_rfc_url()
    const body = document.querySelector('body');
    body.innerHTML = '<div id="propose-version"><a id="html-link" href="' + alt_url  + '">HTML version?</a> <button type="button" id="close-html-version-button" aria-label="Close"> <span aria-hidden="true">&times;</span></button></div>' + body.innerHTML;
    const close_button = document.getElementById('close-html-version-button');
    close_button.addEventListener('click', close_action)
}

function html_rfc_url(){
    return "https://rfc-editor.org/rfc/rfc" + get_rfc_number()
}

function get_rfc_number(){
    const url = window.location.href
    const chunks = url.split("/")
    const filename = chunks[chunks.length -1]
    const regexp = /rfc([0-9]+)\.txt/
    return filename.replace(regexp, '$1')
}


main()
